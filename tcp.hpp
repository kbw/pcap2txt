/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#pragma once

#include "defs.hpp"
#include <ostream>
#include <stddef.h>

struct Options;
struct Buffer;
struct ip_hdr;


//---------------------------------------------------------------------------
//
//    0                   1                   2                   3   
//    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |          Source Port          |       Destination Port        |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                        Sequence Number                        |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                    Acknowledgment Number                      |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |  Data |           |U|A|P|R|S|F|                               |
//   | Offset| Reserved  |R|C|S|S|Y|I|            Window             |
//   |       |           |G|K|H|T|N|N|                               |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |           Checksum            |         Urgent Pointer        |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                    Options                    |    Padding    |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                             data                              |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
#pragma pack(1)
struct tcp_hdr
{
private:
	uint16_t	src_port;
	uint16_t	dst_port;
	uint32_t	seqno;
	uint32_t	ackno;
	uint16_t	flags;
	uint16_t	window;
	uint16_t	checksum;
	uint16_t	urgentptr;
//	uint32_t	options;
//	uint32_t	data;

public:
	enum EFlags
	{
		FIN = 0x0001,
		SYN = 0x0002,
		RST = 0x0004,
		PUSH = 0x008,
		ACK = 0x0010,
		URG = 0x0020
	};

	uint16_t	get_sport()	const	{ return ntohs(src_port); }
	uint16_t	get_dport() const	{ return ntohs(dst_port); }
	uint16_t	get_window() const	{ return ntohs(window); }
	uint16_t	get_checksum() const{ return ntohs(checksum); }

	uint16_t get_flags() const
	{
		uint16_t tmp = flags;
		tmp = (tmp & 0x3f);
		return tmp;
	}
	bool get_flag_FIN() const
	{
		uint16_t tmp = get_flags() & FIN;
		return tmp != 0;
	}
	bool get_flag_SYN() const
	{
		uint16_t tmp = get_flags() & SYN;
		return tmp != 0;
	}
	bool get_flag_RST() const
	{
		uint16_t tmp = get_flags() & RST;
		return tmp != 0;
	}
	bool get_flag_PUSH() const
	{
		uint16_t tmp = get_flags() & PUSH;
		return tmp != 0;
	}
	bool get_flag_ACK() const
	{
		uint16_t tmp = get_flags() & ACK;
		return tmp != 0;
	}
	bool get_flag_URG() const
	{
		uint16_t tmp = get_flags() & URG;
		return tmp != 0;
	}

	uint16_t get_data_offset() const
	{
		uint16_t tmp = ntohs(flags);
		tmp = (tmp >> 12);
		return tmp * sizeof(uint32_t);
	}
};
#pragma pack()

bool handle_tcp(std::ostream &os, const Options &opts, const ip_hdr *ip_hdr, const Buffer &buf, ptrdiff_t offset, size_t level = 0);
