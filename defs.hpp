/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#pragma once

#ifdef _WIN32					// Native Windows
	#include <winsock2.h>
	#define snprintf	_snprintf
#elif defined(__CYGWIN__)		// gcc/cygwin on Windows
	#include <cygwin/in.h>
#elif defined(__linux__)
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <arpa/inet.h>
#else
#endif

#ifdef HAVE_CONFIG_H			// Autobuild header
#include "config.h"
#endif

#ifdef _MSC_VER					// Microsoft C/C++ compiler--no stdint.h
typedef __int64			int64_t;
typedef unsigned __int64 uint64_t;
typedef int				int32_t;
typedef unsigned int	uint32_t;
typedef short			int16_t;
typedef unsigned short	uint16_t;
typedef char			int8_t;
typedef unsigned char	uint8_t;
#else
#include <stdint.h>
#endif

#ifndef PACKAGE_STRING
#define PACKAGE_STRING "pcap2txt 0.3.0"
#endif
