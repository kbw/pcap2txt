/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#pragma once

#include "defs.hpp"

struct Buffer
{
	Buffer(size_t size = 0);
	~Buffer();

	void realloc(size_t n);
	void clear();
	size_t totalsize() const;
	size_t size() const;

	operator char*();
	operator const char*() const;
	operator size_t() const;

	char*	_ptr;
	size_t	_block_size;
	size_t	_size;
};

inline Buffer::Buffer(size_t size)
	:	_ptr(0),
		_block_size(0),
		_size(size)
{
	if (size > 0)
		realloc(size);
}

inline Buffer::~Buffer()
{
	free(_ptr);
}

inline void Buffer::realloc(size_t size)
{
	if (size > _block_size)
	{
		free(_ptr);
		_ptr = reinterpret_cast<char*>(malloc(size));
		_block_size = _size = (_ptr == 0) ? 0 : size;
	}
	else
		_size = size;
}

inline void Buffer::clear()
{
	if (_ptr)
	{
		memset(_ptr, 0, _size);
	}
}

inline size_t Buffer::size() const
{
	return _size;
}

inline Buffer::operator char*()
{
	return _ptr;
}

inline Buffer::operator const char*() const
{
	return _ptr;
}

inline Buffer::operator size_t() const
{
	return _size;
}
