/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#include "Options.hpp"
#include <iostream>


Options::Options()
{
	show_number		=
	show_time		= false;

	show_eth_src	=
	show_eth_dst	=
	show_eth_proto	= false;

	show_ip_src		=
	show_ip_dst		=
	show_ip_ttl		=
	show_ip_DF		=
	show_ip_MF		= false;

	show_tcp_sport	=
	show_tcp_dport	=
	show_tcp_window	=
	show_tcp_FIN	=
	show_tcp_SYN	=
	show_tcp_RST	=
	show_tcp_PUSH	=
	show_tcp_ACK	=
	show_tcp_URG	=
	show_tcp_datalen= false;

	state = File;
}

void Options::operator()(char* arg)
{
	if (arg[0] == '-')
	{
		if (state != File)
			bad_param(arg);

		if (std::string(&arg[1]) == "-version")
		{
			show_version();
		}
		else if (std::string(&arg[1]) == "-help")
		{
			show_help();
		}
		else if (std::string(&arg[1]) == "n")
		{
			++show_number;
		}
		else if (std::string(&arg[1]) == "t")
		{
			++show_time;
		}
		else if (std::string(&arg[1]) == "ETH")
		{
			state = Ethernet;
		}
		else if (std::string(&arg[1]) == "IP")
		{
			state = IP;
		}
		else if (std::string(&arg[1]) == "TCP")
		{
			state = TCP;
		}
		else
			bad_param(arg);
	}
	else
	{
		switch (state)
		{
		case File:
			filenames.push_back(arg);
			break;
		case Ethernet:
			state = File;
			if (std::string(arg) == "all")
				show_eth_src =
				show_eth_dst =
				show_eth_proto = true;
			else if (std::string(arg) == "src")
				++show_eth_src;
			else if (std::string(arg) == "dst")
				++show_eth_dst;
			else if (std::string(arg) == "proto")
				++show_eth_proto;
			else
				bad_param(std::string("-ETH ") + arg);
			break;
		case IP:
			state = File;
			if (std::string(arg) == "all")
				show_ip_src	=
				show_ip_dst	=
				show_ip_ttl	=
				show_ip_DF	=
				show_ip_MF	= true;
			else if (std::string(arg) == "flags")
				show_ip_DF	=
				show_ip_MF	= true;
			else if (std::string(arg) == "src")
				++show_ip_src;
			else if (std::string(arg) == "dst")
				++show_ip_dst;
			else if (std::string(arg) == "ttl")
				++show_ip_ttl;
			else if (std::string(arg) == "DF")
				++show_ip_DF;
			else if (std::string(arg) == "MF")
				++show_ip_MF;
			else
				bad_param(std::string("-IP ") + arg);
			break;
		case TCP:
			state = File;
			if (std::string(arg) == "all")
				show_tcp_sport	=
				show_tcp_dport	=
				show_tcp_window	=
				show_tcp_FIN	=
				show_tcp_SYN	=
				show_tcp_RST	=
				show_tcp_PUSH	=
				show_tcp_ACK	=
				show_tcp_URG	=
				show_tcp_datalen= true;
			else if (std::string(arg) == "flags")
				show_tcp_FIN	=
				show_tcp_SYN	=
				show_tcp_RST	=
				show_tcp_PUSH	=
				show_tcp_ACK	=
				show_tcp_URG	= true;
			else if (std::string(arg) == "sport")
				++show_tcp_sport;
			else if (std::string(arg) == "dport")
				++show_tcp_dport;
			else if (std::string(arg) == "window")
				++show_tcp_window;
			else if (std::string(arg) == "FIN")
				++show_tcp_FIN;
			else if (std::string(arg) == "SYN")
				++show_tcp_SYN;
			else if (std::string(arg) == "RST")
				++show_tcp_RST;
			else if (std::string(arg) == "PUSH")
				++show_tcp_PUSH;
			else if (std::string(arg) == "ACK")
				++show_tcp_ACK;
			else if (std::string(arg) == "URG")
				++show_tcp_URG;
			else if (std::string(arg) == "datalen")
				++show_tcp_datalen;
			else
				bad_param(std::string("-TCP ") + arg);
			break;
		};
	}
}

void Options::bad_param(std::string err)
{
	std::clog << "not expecting: " << err << std::endl;
	exit(1);
}

void show_help()
{
	static const char* text[] = {
		"'pcap2txt' Converts tcpdump pcap files to text.",
		"",
		"Usage: pcap2txt OPTION [[file1], [file2], ..., ]",
		"   or: pcap2txt OPTION",
		"Displays a pcap file.",
		"",
		" -n           Show packet number",
		" -t           Show time",
		"",
		" -ETH all     Show all Ethernet details",
		" -ETH src     Show Ethernet src addr",
		" -ETH dst     Show Ethernet dst addr",
		" -ETH proto   Show Ethernet protcol",
		"",
		" -IP all      Show all IP details",
		" -IP src      Show IP src addr",
		" -IP dst      Show IP dst addr",
		" -IP ttl      Show IP ttl addr",
		" -IP DF       Show IP Don't Fragment flag",
		" -IP MF       Show IP More Fragments flag",
		"",
		" -TCP all     Show all TCP details",
		" -TCP sport   Show TCP src port",
		" -TCP dport   Show TCP dst port",
		" -TCP window  Show TCP window",
		" -TCP FIN     Show TCP FIN flag",
		" -TCP SYN     Show TCP SYN flag",
		" -TCP RST     Show TCP RST flag",
		" -TCP PUSH    Show TCP PUSH flag",
		" -TCP ACK     Show TCP ACK flag",
		" -TCP URG     Show TCP URG flag",
		" -TCP datalen Show TCP data record length",
		"",
		" --version    output version information and exit",
		" --help       display help and exit",
		"",
		"Report bugs to Keith Williams<kwilliams@bgcpartners.com>",
		0
	};

	for (size_t i = 0; text[i]; ++i)
		std::cout << text[i] << std::endl;

	exit(1);
}

void show_version()
{
	static const char* text[] = {
		PACKAGE_STRING,
		"Copyright (C) 2010 eSpeed Ltd.  All rights reserved.",
		"",
		"Written by Keith Williams<kwilliams@bgcpartners.com>",
		0
	};

	for (size_t i = 0; text[i]; ++i)
		std::cout << text[i] << std::endl;

	exit(1);
}
