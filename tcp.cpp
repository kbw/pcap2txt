/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#include "tcp.hpp"
#include "ip.hpp"
#include "Options.hpp"
#include "Buffer.hpp"


bool handle_tcp(std::ostream &os, const Options &opts, const ip_hdr *ip_hdr, const Buffer &buf, ptrdiff_t offset, size_t level)
{
	const tcp_hdr* hdr = reinterpret_cast<tcp_hdr*>(buf._ptr + offset);
	std::string levelstr(" ", level);

	if (opts.show_tcp_sport)	os << " " << hdr->get_sport();
	if (opts.show_tcp_dport)	os << " " << hdr->get_dport();
	if (opts.show_tcp_window)	os << " " << hdr->get_window();
	if (opts.show_tcp_FIN)		os << " " << hdr->get_flag_FIN();
	if (opts.show_tcp_SYN)		os << " " << hdr->get_flag_SYN();
	if (opts.show_tcp_RST)		os << " " << hdr->get_flag_RST();
	if (opts.show_tcp_PUSH)		os << " " << hdr->get_flag_PUSH();
	if (opts.show_tcp_ACK)		os << " " << hdr->get_flag_ACK();
	if (opts.show_tcp_URG)		os << " " << hdr->get_flag_URG();
	if (opts.show_tcp_datalen)	os << " " << (ip_hdr->get_len() - (hdr->get_data_offset() + ip_hdr->get_hdrlen()));

	return 0;
}
