/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

//---------------------------------------------------------------------------
//	pcap2txt	Converts tcpdump files to text.
//---------------------------------------------------------------------------

#include "Options.hpp"
#include "Buffer.hpp"
#include "pcap.hpp"
#include "ethernet.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <time.h>


//---------------------------------------------------------------------------

void process(std::istream &is, const Options &opts);


int main(int argc, char* argv[])
{
	Options opts = std::for_each(&argv[1], &argv[argc], Options());

	if (opts.filenames.empty())
	{
		process(std::cin, opts);
	}
	else
	{
		for (size_t i = 0; i < opts.filenames.size(); ++i)
		{
			const char* filename = opts.filenames[i].c_str();
			std::ifstream is(filename, std::ios::binary);
			process(is, opts);
		}
	}
	return 0;
}


//---------------------------------------------------------------------------

template<typename T>
std::istream& read(std::istream &is, T &rec)
{
	is.read(reinterpret_cast<char*>(&rec), sizeof(T));
	return is;
}

void process(std::istream &is, const Options &opts)
{
	Buffer buf;

	pcap_hdr_t hdr;
	if (read<pcap_hdr_t>(is, hdr) == 0)
		return;

	pcaprec_hdr_t rec;
	for (uint64_t linecnt = 1; read<pcaprec_hdr_t>(is, rec); ++linecnt)
	{
		std::ostringstream os;

		if (struct tm* t = localtime(reinterpret_cast<time_t*>(&rec.ts_sec)))
		{
			if (opts.show_number)
				os << " " << linecnt;

			if (opts.show_time)
			{
				os	<< " " << std::dec
					<< std::setw(2) << std::setfill('0') << (t->tm_year + 1900) << '-'
					<< std::setw(2) << std::setfill('0') << (t->tm_mon + 1) << '-'
					<< std::setw(2) << std::setfill('0') << (t->tm_mday) << ' '
					<< std::setw(2) << std::setfill('0') << (t->tm_hour) << ':'
					<< std::setw(2) << std::setfill('0') << (t->tm_min) << ':'
					<< std::setw(2) << std::setfill('0') << (t->tm_sec) << '.'
					<< std::setw(6) << std::setfill('0') << (rec.ts_usec);
			}

			buf.realloc(rec.incl_len);
			if (buf.size() == 0)
				break;

			is.read(buf, static_cast<std::streamsize>(buf.size()));

			std::ios::pos_type prior = os.tellp();
			handle_ethernet(os, opts, buf);
			std::ios::pos_type post = os.tellp();

			// Skip blank lines
			if (prior != post)
			{
				std::string str = os.str();
				std::cout << &str.c_str()[1] << std::endl;
			}
 		}
		else
		{
			// Advance to the next record
			is.seekg(rec.incl_len, std::ios::cur);
		}
	}
}
