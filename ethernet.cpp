/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#include "ethernet.hpp"
#include "ip.hpp"
#include "arp.hpp"
#include "Options.hpp"
#include "Buffer.hpp"
#include <sstream>
#include <iomanip>


bool handle_ethernet(std::ostream &os, const Options &opts, const Buffer &buf, ptrdiff_t offset, size_t level)
{
	const ethernet_hdr* hdr = reinterpret_cast<ethernet_hdr*>(buf._ptr + offset);
	std::string levelstr(" ", level);

	if (opts.show_eth_src)	os << " " << hdr->get_dst_text();
	if (opts.show_eth_dst)	os << " " << hdr->get_src_text();
	if (opts.show_eth_proto)os << " " << hdr->get_protocol_text();

	if (hdr->get_protocol() == ethernet_hdr::IP)
		handle_ip(os, opts, buf, offset + sizeof(ethernet_hdr), level + 1);
	else if (hdr->get_protocol() == ethernet_hdr::ARP)
		handle_arp(os, opts, buf, offset + sizeof(ethernet_hdr), level + 1);

	return 0;
}

std::string ethernet_hdr::mac_addr_to_text(const uint8_t addr[6], size_t size)
{
	std::ostringstream os;

	for (size_t i = 0; i < size; ++i)
	{
		if (i != 0)
			os << ':';
		os << std::hex << std::setw(2) << std::setfill('0') << (int)addr[i];
	}

	return os.str();
}

std::string ethernet_hdr::mac_protocol_to_text(uint16_t protcol)
{
	std::ostringstream os;

	os	<< "0x"
		<< std::hex << std::setw(4) << std::setfill('0') << protcol;

	return os.str();
}
