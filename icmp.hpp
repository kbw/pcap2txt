/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#pragma once

#include "defs.hpp"
#include <ostream>
#include <stddef.h>

struct Options;
struct Buffer;
struct ip_hdr;


bool handle_icmp(std::ostream &os, const Options &opts, const ip_hdr *ip_hdr, const Buffer &buf, ptrdiff_t offset, size_t level);
