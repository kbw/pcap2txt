/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#pragma once

#include "defs.hpp"

#pragma pack(1)
struct pcap_hdr_t
{
	pcap_hdr_t() { memset(this, 0, sizeof(*this)); }

	uint32_t magic_number;   /* magic number */
	uint16_t version_major;  /* major version number */
	uint16_t version_minor;  /* minor version number */
	int32_t  thiszone;       /* GMT to local correction */
	uint32_t sigfigs;        /* accuracy of timestamps */
	uint32_t snaplen;        /* max length of captured packets, in octets */
	uint32_t network;        /* data link type */
};

struct pcaprec_hdr_t
{
	pcaprec_hdr_t() { memset(this, 0, sizeof(*this)); }

	uint32_t ts_sec;         /* timestamp seconds */
	uint32_t ts_usec;        /* timestamp microseconds */
	uint32_t incl_len;       /* number of octets of packet saved in file */
	uint32_t orig_len;       /* actual length of packet */
};
#pragma pack()
