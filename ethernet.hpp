/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#pragma once

#include "defs.hpp"
#include <string>
#include <ostream>
#include <stddef.h>

struct Options;
struct Buffer;

//---------------------------------------------------------------------------
//
//    0                   1                   2                   3   
//    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |  Destination Address  |    Source Address     |   Id  |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
#pragma pack(1)
struct ethernet_hdr
{
protected:
	uint8_t		dst_addr[6];
	uint8_t		src_addr[6];
	uint16_t	protcol;

	static std::string mac_addr_to_text(const uint8_t addr[6], size_t size);
	static std::string mac_protocol_to_text(uint16_t protcol);

public:
	enum EProtocol { IP = 0x0800, ARP = 0x0806 };

	const uint8_t*	get_dst() const			{ return dst_addr; }
	const uint8_t*	get_src() const			{ return src_addr; }
	size_t			get_dst_size() const	{ return sizeof(dst_addr); }
	size_t			get_src_size() const	{ return sizeof(src_addr); }
	std::string		get_dst_text() const	{ return mac_addr_to_text(get_dst(), get_dst_size()); }
	std::string		get_src_text() const	{ return mac_addr_to_text(get_src(), get_src_size()); }
	uint16_t		get_protocol() const	{ return htons(protcol); }
	std::string		get_protocol_text()const{ return mac_protocol_to_text(get_protocol()); }
};
#pragma pack()

bool handle_ethernet(std::ostream &os, const Options &opts, const Buffer &buf, ptrdiff_t offset = 0, size_t = 1);
