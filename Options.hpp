/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#pragma once

#include "defs.hpp"
#include <string>
#include <vector>

struct Options
{
	typedef std::string filename_t;
	typedef std::vector<filename_t> filenames_t;

	enum EState { File, Ethernet, IP, TCP };

	bool	show_number;
	bool	show_time;

	bool	show_eth_src;
	bool	show_eth_dst;
	bool	show_eth_proto;

	bool	show_ip_src;
	bool	show_ip_dst;
	bool	show_ip_ttl;
	bool	show_ip_DF;
	bool	show_ip_MF;

	bool	show_tcp_sport;
	bool	show_tcp_dport;
	bool	show_tcp_window;
	bool	show_tcp_FIN;
	bool	show_tcp_SYN;
	bool	show_tcp_RST;
	bool	show_tcp_PUSH;
	bool	show_tcp_ACK;
	bool	show_tcp_URG;
	bool	show_tcp_datalen;

	EState	state;
	filenames_t filenames;

	Options();
	void operator()(char*);

private:
	static void bad_param(std::string err);
};

void show_version();
void show_help();
