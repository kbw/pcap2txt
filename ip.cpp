/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#include "ip.hpp"
#include "tcp.hpp"
#include "udp.hpp"
#include "icmp.hpp"
#include "Options.hpp"
#include "Buffer.hpp"


bool handle_ip(std::ostream &os, const Options &opts, const Buffer &buf, ptrdiff_t offset, size_t level)
{
	const ip_hdr* hdr = reinterpret_cast<ip_hdr*>(buf._ptr + offset);
	std::string levelstr(" ", level);

	if (opts.show_ip_src)	os << " " << hdr->get_src_addr_text();
	if (opts.show_ip_dst)	os << " " << hdr->get_dst_addr_text();
	if (opts.show_ip_ttl)	os << " " << hdr->get_ttl();
	if (opts.show_ip_DF)	os << " " << hdr->get_flag_DF();
	if (opts.show_ip_MF)	os << " " << hdr->get_flag_MF();

	if (hdr->get_protocol() == ip_hdr::TCP)
	{
		handle_tcp(os, opts, hdr, buf, offset + hdr->get_hdrlen(), level + 1);
	}
	else if (hdr->get_protocol() == ip_hdr::UDP)
	{
		handle_udp(os, opts, hdr, buf, offset + hdr->get_hdrlen(), level + 1);
	}
	else if (hdr->get_protocol() == ip_hdr::ICMP)
	{
		handle_icmp(os, opts, hdr, buf, offset + hdr->get_hdrlen(), level + 1);
	}

	return 0;
}
