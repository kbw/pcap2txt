/*---------------------------------------------------------------------------
	Copyright (c) 2010 Keith Williams.
	All rights reserved.

	Redistribution and use in source and binary forms are permitted
	provided that the above copyright notice and this paragraph are
	duplicated in all such forms and that any documentation,
	advertising materials, and other materials related to such
	distribution and use acknowledge that the software was developed
	by the Webbusy Ltd.
  ---------------------------------------------------------------------------*/

#pragma once

#include "defs.hpp"
#include <string>
#include <ostream>
#include <stddef.h>

struct Options;
struct Buffer;


//---------------------------------------------------------------------------
//
//    0                   1                   2                   3   
//    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |Version|hdr len| tos           | total len                     |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |            Id                 |R|D|M|       Offset            |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |       ttl     |    protocol   |         hdr checksum          |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                        Source Address                         |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                      Destination Address                      |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                      Options and Padding                      |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
//	 tos - 	(0 - 2) Precedence, Low Delay, High Throughput, High Reliability
//    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |               |D|T|R|                                         |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
//	 control flags -	R - Reserved, D - Don't Fragment, M - More Fragments
//    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                               |R|D|M|                         |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

#pragma pack(1)
struct ip_hdr
{
private:
	enum { size = 6*4 };

	uint8_t			hdr_ver;
	uint8_t			tos;
	uint16_t		total_len;
	uint16_t		id;
	uint16_t		offset;
	uint8_t			ttl;
	uint8_t			protocol;	// 6-tcp, 17-udp, 1-icmp
	uint16_t		hdrchecksum;
	struct in_addr	src_addr;
	struct in_addr	dst_addr;
//	uint32_t		padding;

public:
	enum EProtocol	{ TCP = 6, UDP = 17, ICMP = 1, PIM = 0x67 };
	enum EFlag		{ MASK = 0xA000, DF = 0x2, MF = 0x4 };

	//	111 - Network Control
	//	110 - Internetwork Control
	//	101 - CRITIC/ECP
	//	100 - Flash Override
	//	011 - Flash
	//	010 - Immediate
	//	001 - Priority
	//	000 - Routine
	enum ETOSPrecedence
	{
		ERoutine = 0,
		EPriority = 1,
		EImmediate = 2,
		EFlash = 3,
		EFlashOverride = 4,
		ECritical = 5,
		EIC = 6,
		ENC = 7
	};

	uint8_t			get_hdrlen() const				{ return (hdr_ver & 0x0F) * sizeof(uint32_t); }
	uint8_t			get_ver() const					{ return (hdr_ver & 0xF0) >> 4; }
	uint8_t			get_tos() const					{ return tos; }
	uint8_t			get_tos_precedence() const		{ return get_tos() >> 5; }
	bool			get_tos_lowdelay() const		{ return (get_tos() & 0x10) != 0; }
	bool			get_tos_highthroughput() const	{ return (tos & 0x08) != 0; }
	bool			get_tos_highreliability() const	{ return (tos & 0x04) != 0; }
	uint16_t		get_len() const					{ return ntohs(total_len); }
	uint16_t		get_id() const					{ return ntohs(id); }
	uint8_t			get_flags() const				{ return ntohs(offset) >> 13; }
	bool			get_flag_DF() const				{ return (get_flags() & DF) != 0; }
	bool			get_flag_MF() const				{ return (get_flags() & MF) != 0; }
	uint16_t		get_offset() const				{ return ntohs(offset) & 0x1fff; }
	uint16_t		get_ttl() const					{ return ntohs(ttl); }
	uint8_t			get_protocol() const			{ return protocol; }
	uint16_t		get_hdrchecksum() const			{ return ntohs(hdrchecksum); }
	struct in_addr	get_src_addr() const			{ return src_addr; }
	struct in_addr	get_dst_addr() const			{ return dst_addr; }
	std::string		get_src_addr_text() const		{ return get_addr_text(src_addr); }
	std::string		get_dst_addr_text() const		{ return get_addr_text(dst_addr); }

	static std::string get_addr_text(const struct in_addr &addr)
	{
		uint32_t a = ntohl(addr.s_addr);

		char buffer[4*3 + 3*1 + 1];	// four 3 digit number + 3 dots + null
		snprintf(buffer, sizeof(buffer), "%d.%d.%d.%d",
			(a >> 24) & 0x000000ff,
			(a >> 16) & 0x000000ff,
			(a >>  8) & 0x000000ff,
			(a      ) & 0x000000ff);
		buffer[sizeof(buffer) - 1] = 0;	// force terminator

		return buffer;
	}
};
#pragma pack()

bool handle_ip(std::ostream &os, const Options &opts, const Buffer &buf, ptrdiff_t offset = 0, size_t level = 0);
